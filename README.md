# Employee Guide

This is an implementation of a simple app which can connect to a JSON API to retrieve a list of employee records and display their detail. The main implementation here simply reads in a JSON file locally but has the skeleton code required to connect to a live API using Retrofit. **When running this app _AS-IS_, you should select the `offlineApi` product flavor since the online variant is not implemented with a live API.**

## Build / Software Requirements

* Android Studio with gradle version 2.1.3
* Android SDK build tools version 24.0.2
* Android SDK v24 (Nougat) [but it should compile with API23 if you 
don't already have 24 on your machine]
* All other dependencies (libraries, etc) are listed in the app's 
`build.gradle` files
* Min SDK for running set at API 17

## Build & Use

There are two product flavors included:

  * `onlineApi` - this can be configured to connect to a live API when 
  made available
  * `offlineApi` - simply reads some JSON from a local resource.

Select the `offlineApi` flavor in the Build Variants window in Android Studio and then 'Run/Debug' to run the sample app.

## Overview of the app
### User Interface

There are two main screens:

* The Employees List (`MainEmployeesActivity`)
    * Overview grid of employees with picture (or placeholder) and name
* Employee profile (`EmployeeProfileActivity`
    * More info on each employee

I aimed to follow google's Material Design principles and made use of shared element Activity Transitions to give the user a feeling of continuity - watch for how the avatar moves between the list and the detail screens. I made use of the `CoordinatorLayout` with a collapsing toolbar in the detail view - this would be particularly useful if the API is extended to include more data about each employee, allowing the user to slide away the avatar and focus on the particulars.

### Architecture

Each screen is implemented using a Model-View-Presenter architecture to separate concerns cleanly and avoid business logic in the android `Activity`s. I aimed to keep the views as stupid as possible in terms of what they should display and allow the presenter to control what should be shown on screen. The presenter for the `MainEmployeesActivity` also instantiates the access to the API (the Model) which is currently implemented using Retrofit in the `onlineApi` product flavor. Note, however, that I abstracted the Retrofit interface behind the `EmployeesClient` interface so that the Retrofit library could be swapped in for another library (or even a secondary offline-mode implementation) at a later date.


