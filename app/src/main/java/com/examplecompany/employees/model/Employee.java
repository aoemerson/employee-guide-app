package com.examplecompany.employees.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Employee implements Parcelable {

    public static final Parcelable.Creator<Employee> CREATOR = new Parcelable.Creator<Employee>() {
        @Override
        public Employee createFromParcel(Parcel source) {
            return new Employee(source);
        }

        @Override
        public Employee[] newArray(int size) {
            return new Employee[size];
        }
    };
    Profile profile;

    public Employee() {

    }

    public Employee(Profile profile) {
        this.profile = profile;
    }

    protected Employee(Parcel in) {
        this.profile = in.readParcelable(Profile.class.getClassLoader());
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.profile, flags);
    }
}
