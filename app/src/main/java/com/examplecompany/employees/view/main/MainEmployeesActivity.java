package com.examplecompany.employees.view.main;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Fade;
import android.transition.Transition;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;

import com.examplecompany.employees.R;
import com.examplecompany.employees.model.Employee;
import com.examplecompany.employees.presenter.main.MainEmployeesPresenter;
import com.examplecompany.employees.presenter.main.EmployeesPresenter;
import com.examplecompany.employees.view.ActionCommand;
import timber.log.Timber;

public class MainEmployeesActivity extends AppCompatActivity implements EmployeesView, EmployeeAdapter.OnItemClickListener {

    private static final String OUT_STATE_PRESENTER = "OUT_STATE_PRESENTER";
    @BindView(R.id.recyclerview_employees_grid) RecyclerView employeesGridView;
    @BindInt(R.integer.employee_grid_columns) int employeeGridColumns;
    @BindView(R.id.progressBar) ProgressBar progressBar;

    private EmployeeAdapter employeeAdapter;
    private EmployeesPresenter presenter;
    private GridLayoutManager gridLayoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_employees);
        ButterKnife.bind(this);
        configureTransitions();

        employeeAdapter = new EmployeeAdapter(this);
        employeesGridView.setAdapter(employeeAdapter);
        gridLayoutManager = new GridLayoutManager(this, employeeGridColumns);
        employeesGridView.setHasFixedSize(true);
        employeesGridView.setLayoutManager(gridLayoutManager);
        initPresenter(savedInstanceState);
    }

    private void configureTransitions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Transition fade = new Fade();
            fade.excludeTarget(android.R.id.navigationBarBackground, true);
            getWindow().setExitTransition(fade);

            getWindow().setEnterTransition(fade);
        }
    }

    private void initPresenter(Bundle savedInstanceState) {
        if (savedInstanceState != null && savedInstanceState.containsKey(OUT_STATE_PRESENTER)) {
            presenter = savedInstanceState.getParcelable(OUT_STATE_PRESENTER);
            presenter.restore();
        } else {
            presenter = new MainEmployeesPresenter();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(OUT_STATE_PRESENTER, ((MainEmployeesPresenter) presenter));
    }


    @Override
    protected void onPause() {
        super.onPause();
        presenter.detachView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.attachView(this);
        presenter.requestEmployees();
    }

    @Override
    public void showEmployees(List<Employee> employees) {
        employeeAdapter.setEmployees(employees);
    }

    @Override
    public void showEmployeeDetail(ActionCommand command) {
        Intent intent = command.getIntent(this);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            View ribotItemView = gridLayoutManager.findViewByPosition(command.getDataPosition());
            View avatar = ribotItemView.findViewById(R.id.imageview_avatar);

            ActivityOptions activityOptions = ActivityOptions
                    .makeSceneTransitionAnimation(this, avatar, "avatar_transition");
            startActivity(intent, activityOptions.toBundle());
        } else {
            startActivity(intent);
        }
    }

    @Override
    public void showLoading(boolean loading) {
        Timber.d("showLoading() called with: loading = [%s]",loading);
        progressBar.setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showError(int errorMsgResId) {
        Toast.makeText(MainEmployeesActivity.this, getString(errorMsgResId), Toast.LENGTH_SHORT)
             .show();
    }

    @Override
    public void onEmployeeItemClicked(int itemPosition) {
        presenter.employeeClicked(itemPosition);
    }
}
