package com.examplecompany.employees.view;

/**
 * Created by Andrew on 09/09/2016.
 */
public interface BaseView {

    /**
     * Shows a loading indication on the display
     * @param loading {@code true} if loading, {@code false} otherwise
     */
    void showLoading(boolean loading);

    /**
     * Shows an error message on the display
     * @param errorMsgResId the android String resource id of the message to show
     */
    void showError(int errorMsgResId);


}
