package com.examplecompany.employees.view;

import android.app.Activity;
import android.content.Intent;

/**
 * An interface describing a transition between one view and another from Master to Detail. The
 * receiving view uses this to launch the next action without needing the know the implementation
 * details.
 *
 * For example, if the View is an {@link Activity}, it can simply do:
 * <br>
 * <br>
 * <code>
 *     ActionCommand action; // some action<br>
 *     startActivity(action.getIntent(this));
 * </code>
 */
public interface ActionCommand {

    void execute(Activity activity);

    Intent getIntent(Activity originActivity);

    int getDataPosition();
}
