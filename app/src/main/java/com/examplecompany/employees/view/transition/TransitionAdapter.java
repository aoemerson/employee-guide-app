package com.examplecompany.employees.view.transition;

import android.annotation.TargetApi;
import android.os.Build;
import android.transition.Transition;
import android.transition.Transition.TransitionListener;

/**
 * A convenience implementation of {@link TransitionListener} to allow easy overriding of a
 * subset of its methods.
 */
@TargetApi(Build.VERSION_CODES.KITKAT)
public class TransitionAdapter implements TransitionListener {


    @Override
    public void onTransitionStart(Transition transition) {

    }

    @Override
    public void onTransitionEnd(Transition transition) {

    }

    @Override
    public void onTransitionCancel(Transition transition) {

    }

    @Override
    public void onTransitionPause(Transition transition) {

    }

    @Override
    public void onTransitionResume(Transition transition) {

    }
}
