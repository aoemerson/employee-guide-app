package com.examplecompany.employees.view.main;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import com.examplecompany.employees.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.examplecompany.employees.model.Employee;

public class EmployeeAdapter extends RecyclerView.Adapter<EmployeeAdapter.EmployeeViewHolder> {

    class EmployeeViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageview_avatar) ImageView avatar;
        @BindView(R.id.textview_employee_name) TextView name;
        @BindView(R.id.view_employee_color_separator) View separator;


        private OnItemClickListener clickListener;
        private int color;

        public EmployeeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.layout_employee_container)
        void onItemClicked() {
            if (clickListener != null)
                clickListener.onEmployeeItemClicked(getAdapterPosition());
        }

        public void bind(Employee employee, final OnItemClickListener clickListener) {
            this.clickListener = clickListener;
            name.setText(employee.getProfile().getName().getFirst());
            setAccentColor(employee);
            loadAvatar(employee);
        }

        private void loadAvatar(Employee employee) {
            String avatarUrl = employee.getProfile().getAvatar();
            if (avatarUrl != null && avatarUrl.length() > 0) {
                Picasso.with(itemView.getContext())
                       .load(avatarUrl)
                       .into(this.avatar);
            } else {
                avatar.setBackgroundColor(color);
            }
        }


        private void setAccentColor(Employee employee) {
            try {
                color = Color.parseColor(employee.getProfile().getHexColor());
//                avatar.setBackgroundColor(ribotColor);
                separator.setBackgroundColor(color);
            } catch (IllegalArgumentException ignored) {
                // Failure to parse colour ignored since we can keep default background colour
                // TODO: Consider logging exception to analytics service like Crashlytics
            }
        }
    }

    interface OnItemClickListener {

        void onEmployeeItemClicked(int itemPosition);
    }

    List<Employee> employees;
    private OnItemClickListener clickListener;

    public EmployeeAdapter(OnItemClickListener listener) {
        this();
        clickListener = listener;
    }

    public EmployeeAdapter() {
        employees = new ArrayList<>();
    }

    public void setClickListener(OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
        notifyDataSetChanged();
    }

    @Override
    public EmployeeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                               .inflate(R.layout.item_employee, parent, false);
        return new EmployeeViewHolder(v);
    }

    @Override
    public void onBindViewHolder(EmployeeViewHolder holder, int position) {
        Employee employee = employees.get(position);
        holder.bind(employee, clickListener);
    }

    @Override
    public int getItemCount() {
        return employees.size();
    }


}
