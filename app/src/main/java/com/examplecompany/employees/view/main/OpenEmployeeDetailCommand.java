package com.examplecompany.employees.view.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.examplecompany.employees.model.Employee;
import com.examplecompany.employees.view.ActionCommand;
import com.examplecompany.employees.view.detail.EmployeeProfileActivity;


public class OpenEmployeeDetailCommand implements ActionCommand {


    private final Employee employee;
    private int employeeIndex;

    public OpenEmployeeDetailCommand(Employee employee) {
        this.employee = employee;
    }

    public OpenEmployeeDetailCommand(Employee employee, int employeeIndex) {
        this.employee = employee;
        this.employeeIndex = employeeIndex;
    }

    @Override
    public void execute(Activity activity) {
        activity.startActivity(getIntent(activity));
    }

    @Override
    public Intent getIntent(Activity activity) {
        Intent intent = new Intent(activity, EmployeeProfileActivity.class);
        Bundle extras = new Bundle();
        extras.putParcelable(EmployeeProfileActivity.EXTRA_EMPLOYEE, employee);
        intent.putExtras(extras);
        return intent;
    }

    @Override
    public int getDataPosition() {
        return employeeIndex;
    }


}
