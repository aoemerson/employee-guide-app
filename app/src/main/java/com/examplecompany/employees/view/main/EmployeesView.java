package com.examplecompany.employees.view.main;

import android.app.Activity;
import android.content.Intent;

import java.util.List;

import com.examplecompany.employees.model.Employee;
import com.examplecompany.employees.view.ActionCommand;
import com.examplecompany.employees.view.BaseView;


public interface EmployeesView extends BaseView {

    /**
     * Display the employees on the view
     * @param employees a list of {@link Employee} items to display on the view
     */
    void showEmployees(List<Employee> employees);

    /**
     * Show the employee detail using the detail provided in {@code action}
     * @param action the object describing how this view should display the next view. Typically
     *               this will include an {@link Intent} which an {@link Activity} can start.
     */
    void showEmployeeDetail(ActionCommand action);

}
