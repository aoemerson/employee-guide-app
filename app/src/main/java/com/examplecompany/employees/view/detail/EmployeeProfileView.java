package com.examplecompany.employees.view.detail;

import com.examplecompany.employees.model.Employee;
import com.examplecompany.employees.view.BaseView;

public interface EmployeeProfileView extends BaseView {

    /**
     * Gets the bundlded Employee argument which has been given to this View on its creation.
     * @return the employee
     */
    Employee getEmployeeArgument();

    /**
     * Gets the key used to find the Employee argument
     * @return the key
     */
    String getEmployeeArgumentKey();

    /**
     * Displaysthe Employee name on this view
     * @param name the employee name
     */
    void setName(CharSequence name);

    /**
     * Sets the avatar location URI for this view
     * @param location
     */
    void setAvatar(String location);

    /**
     * Sets the avatar color
     * @param color the color in HTML hex-notation string
     */
    void setAvatarColor(String color);

    /**
     * Displays the email address on the view
     * @param email the email address
     */
    void setEmail(CharSequence email);

    /**
     * Displays the employee bio this view
     * @param bio the employee bio
     */
    void setBio(CharSequence bio);

    /**
     * Displays the employee date of birth on this view
     * @param dob
     */
    void setDateOfBirth(CharSequence dob);

    /**
     * Sets the accent color of this screen
     * @param hexColor the color in HTML hex-notation string
     */
    void setAccentColor(String hexColor);

    /**
     * Notifies the view that all available data has been supplied to it
     */
    void detailFinalised();

    /**
     * Notifies the view to invoke the platforms send-email intent.
     * @param email the target email address
     */
    void sendEmail(String email);
}
