package com.examplecompany.employees.api;

import java.util.List;

import com.examplecompany.employees.model.Employee;
import com.examplecompany.employees.application.EventBus;


public interface EmployeesClient {

    /**
     * A utility class for wrapping Employee loading events which are posted to the
     * {@link com.examplecompany.employees.application.EventBus}
     */
    class EmployeesLoadedEvent {
        private List<Employee> employees;

        public EmployeesLoadedEvent(List<Employee> employees) {
            this.employees = employees;
        }

        public List<Employee> getEmployees() {
            return employees;
        }
    }

    class EmployeeLoadingErrorEvent {
        private Throwable throwable;

        public EmployeeLoadingErrorEvent(Throwable throwable) {
            this.throwable = throwable;
        }

        public Throwable getError() {
            return throwable;
        }
    }

    /**
     * Requests the client to get the Employees for the data provider (online or offline), the
     * results of which will be posted on the {@link EventBus} using {@link EmployeesLoadedEvent} or
     * {@link EmployeeLoadingErrorEvent}
     */
    void getEmployees();
}
