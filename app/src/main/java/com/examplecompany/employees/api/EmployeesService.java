package com.examplecompany.employees.api;

import java.util.List;

import com.examplecompany.employees.model.Employee;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import retrofit2.http.GET;

public interface EmployeesService {

    class Creator {

        public static EmployeesService build() {
            return new Retrofit.Builder()
                    .client(new OkHttpClient.Builder()
                            .build())
                    .addConverterFactory(JacksonConverterFactory.create())
                    .baseUrl(BASE_URL)
                    .build()
                    .create(EmployeesService.class);
        }
    }

    // Replace the below URL with service providing
    String BASE_URL = "";

    @GET("employees")
    Call<List<Employee>> getEmployees();


}
