package com.examplecompany.employees.presenter;

import com.examplecompany.employees.view.BaseView;

public interface BasePresenter<T extends BaseView> {

    /**
     * Called by implementations of {@link BaseView} to attach themselves to the presenter
     * @param view the view
     */
    void attachView(T view);

    /**
     * Called by implementations of {@link BaseView} to detach themselves from the presenter
     */
    void detachView();
}
