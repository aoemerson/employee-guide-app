package com.examplecompany.employees.presenter.detail;

import android.support.annotation.NonNull;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.examplecompany.employees.model.Name;
import com.examplecompany.employees.model.Profile;
import com.examplecompany.employees.model.Employee;
import com.examplecompany.employees.view.detail.EmployeeProfileView;

public class MainEmployeeProfilePresenter implements EmployeeProfilePresenter {

    private EmployeeProfileView employeeProfileView;
    private Employee employee;

    @Override
    public void requestData() {
        employee = employeeProfileView.getEmployeeArgument();
        if (employee != null) {
            Profile profile = employee.getProfile();
            if (profile != null) {
                setAvatar(profile);
                setName(profile);
                setDob(profile);
                setEmail(profile);
                setBio(profile);
                employeeProfileView.setAccentColor(profile.getHexColor());
                employeeProfileView.detailFinalised();
            }
        } else {
//       TODO: handle this error
        }

    }

    @Override
    public void onEmailButtonClicked() {
        employeeProfileView.sendEmail(employee.getProfile().getEmail());
    }

    private void setAvatar(Profile profile) {
        String avatar = profile.getAvatar();
        if (avatar != null && avatar.length() > 0)
            employeeProfileView.setAvatar(avatar);
        else {
            employeeProfileView.setAvatarColor(profile.getHexColor());
        }
    }

    private void setBio(Profile profile) {
        String bio = profile.getBio();
        if (validField(bio)) {
            employeeProfileView.setBio(bio);
        }
    }

    private void setEmail(@NonNull Profile profile) {
        String email = profile.getEmail();
        if (validField(email)) {
            employeeProfileView.setEmail(email);
        }

    }

    private void setDob(@NonNull Profile profile) {
        Date dob = profile.getDateOfBirth();
        if (dob != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", Locale.US);
            String dobString = sdf.format(dob);
            employeeProfileView.setDateOfBirth(dobString);
        }
    }

    private void setName(@NonNull Profile profile) {
        Name name = profile.getName();
        if (name != null && validField(name.getFirst())) {
            employeeProfileView.setName(String
                    .format("%s %s", name.getFirst(), name.getLast()));
        }
    }

    private boolean validField(String field) {
        return field != null && field.length() > 0;
    }

    @Override
    public void attachView(EmployeeProfileView view) {
        employeeProfileView = view;
    }

    @Override
    public void detachView() {
        employeeProfileView = null;
    }
}
