package com.examplecompany.employees.presenter.detail;

import com.examplecompany.employees.presenter.BasePresenter;
import com.examplecompany.employees.view.detail.EmployeeProfileView;

public interface EmployeeProfilePresenter extends BasePresenter<EmployeeProfileView> {

    /**
     * Requests the presenter to send back data which the calling View should display
     */
    void requestData();

    /**
     * Notify the presenter that the email button on the {@link EmployeeProfileView} has
     * been clicked.
     */
    void onEmailButtonClicked();

}
