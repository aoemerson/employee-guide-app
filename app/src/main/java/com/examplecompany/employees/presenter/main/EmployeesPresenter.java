package com.examplecompany.employees.presenter.main;

import com.examplecompany.employees.api.EmployeesClient;
import com.examplecompany.employees.application.EventBus;
import com.examplecompany.employees.model.Employee;
import com.examplecompany.employees.presenter.BasePresenter;
import com.examplecompany.employees.view.main.EmployeesView;
import com.squareup.otto.Subscribe;


public interface EmployeesPresenter extends BasePresenter<EmployeesView> {

    /**
     * Invoked by the {@link EmployeesView} to signal that an Employee item has been clicked
     *
     * @param employeePosition the position of the employee in the data set
     */
    void employeeClicked(int employeePosition);

    /**
     * Invoked by the {@link EmployeesView} to request Employee information to populate its display
     */
    void requestEmployees();

    /**
     * Restores the presenter following a configuration change.
     */
    void restore();

    /**
     * Responds to the event from the {@link EventBus} when the Model client has obtained the
     * {@link Employee} data. Subclasses must include the @{@link Subscribe} annotation and register
     * to the {@link EventBus} in order to receive this event.
     *
     * @param event the event containing the retrieved {@link Employee} data
     */
    void onEmployeesLoaded(EmployeesClient.EmployeesLoadedEvent event);

    /**
     * Responds to the event from the {@link EventBus} when the Model client encountered an error.
     * Subclasses must include the @{@link Subscribe} annotation and register to the
     * {@link EventBus} in order to receive this event.
     * @param errorEvent the event containing the error {@link Throwable}
     */
    void onEmployeesLoadError(EmployeesClient.EmployeeLoadingErrorEvent errorEvent);

}
