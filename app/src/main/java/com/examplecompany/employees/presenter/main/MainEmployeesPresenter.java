package com.examplecompany.employees.presenter.main;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.IOException;
import java.util.List;

import com.examplecompany.employees.R;
import com.examplecompany.employees.api.EmployeesClient;
import com.examplecompany.employees.api.EmployeesClientImpl;
import com.examplecompany.employees.application.EventBus;
import com.examplecompany.employees.model.Employee;
import com.examplecompany.employees.view.main.OpenEmployeeDetailCommand;
import com.examplecompany.employees.view.main.EmployeesView;
import com.squareup.otto.Subscribe;


public class MainEmployeesPresenter implements EmployeesPresenter, Parcelable {

    public static final Parcelable.Creator<MainEmployeesPresenter> CREATOR = new Parcelable.Creator<MainEmployeesPresenter>() {
        @Override
        public MainEmployeesPresenter createFromParcel(Parcel source) {
            return new MainEmployeesPresenter(source);
        }

        @Override
        public MainEmployeesPresenter[] newArray(int size) {
            return new MainEmployeesPresenter[size];
        }
    };

    EmployeesView employeesView;
    EmployeesClient employeesClient;
    List<Employee> employees;

    MainEmployeesPresenter(EmployeesView employeesView, EmployeesClient employeesClient) {
        attachView(employeesView);
        this.employeesClient = employeesClient;
    }

    @Override
    public void attachView(EmployeesView view) {
        EventBus.getInstance().register(this);
        employeesView = view;
    }

    @Override
    public void detachView() {
        EventBus.getInstance().unregister(this);
        employeesView = null;
    }

    public MainEmployeesPresenter(EmployeesView employeesView) {
        attachView(employeesView);
        this.employeesClient = EmployeesClientImpl.getInstance();
    }

    public MainEmployeesPresenter() {
        this.employeesClient = EmployeesClientImpl.getInstance();
    }

    protected MainEmployeesPresenter(Parcel in) {
        this.employees = in.createTypedArrayList(Employee.CREATOR);
    }

    @Override
    public void employeeClicked(int employeePosition) {
        OpenEmployeeDetailCommand detailCommand = new OpenEmployeeDetailCommand(employees
                .get(employeePosition), employeePosition);
        employeesView.showEmployeeDetail(detailCommand);
    }

    @Override
    public void requestEmployees() {

        if (employees == null || employees.size() == 0) {
            employeesView.showLoading(true);
            employeesClient.getEmployees();
        } else {
            employeesView.showEmployees(employees);
        }
    }

    @Override
    public void restore() {
        this.employeesClient = EmployeesClientImpl.getInstance();
    }

    @Override
    @Subscribe
    public void onEmployeesLoaded(EmployeesClient.EmployeesLoadedEvent event) {
        List<Employee> employees = event.getEmployees();
        employeesView.showLoading(false);
        if (employees != null && employees.size() > 0) {
            this.employees = employees;
            employeesView.showEmployees(employees);
        } else {
            employeesView.showError(R.string.error_msg_no_employees_found);
        }
    }

    @Override
    @Subscribe
    public void onEmployeesLoadError(EmployeesClient.EmployeeLoadingErrorEvent errorEvent) {
        employeesView.showLoading(false);
        Throwable e = errorEvent.getError();
        if (e instanceof IOException) {
            employeesView.showError(R.string.error_msg_employee_connection_failed);
        } else {
            employeesView.showError(R.string.error_msg_generic);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.employees);
    }
}
