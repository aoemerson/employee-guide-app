package com.examplecompany.employees.application;

import android.app.Application;

import timber.log.BuildConfig;
import timber.log.Timber;

public class EmployeesApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }
}
