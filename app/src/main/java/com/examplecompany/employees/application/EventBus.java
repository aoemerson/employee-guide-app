package com.examplecompany.employees.application;

import com.squareup.otto.Bus;


public class EventBus extends Bus {

    private static final Bus BUS = new Bus();

    public static Bus getInstance() {
        return BUS;
    }
}
