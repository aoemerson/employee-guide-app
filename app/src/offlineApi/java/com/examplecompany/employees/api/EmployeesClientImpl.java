package com.examplecompany.employees.api;

import com.examplecompany.employees.application.EventBus;
import com.examplecompany.employees.model.Employee;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;


public class EmployeesClientImpl implements EmployeesClient {

    private static EmployeesClientImpl instance;

    private EmployeesClientImpl() {

    }

    public static EmployeesClientImpl getInstance() {
        if (instance == null) {
            instance = new EmployeesClientImpl();
        }
        return instance;
    }

    @Override
    public void getEmployees() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            List<Employee> employees = objectMapper.readValue(getClass()
                            .getResourceAsStream("/response.json"),
                    new TypeReference<List<Employee>>() {});
            EventBus.getInstance().post(new EmployeesLoadedEvent(employees));
        } catch (IOException e) {
           EventBus.getInstance().post(new EmployeeLoadingErrorEvent(e));
        }
    }


}
