package com.examplecompany.employees.util;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.examplecompany.employees.model.Employee;
import com.examplecompany.employees.model.Name;
import com.examplecompany.employees.model.Profile;

/**
 * Created by Andrew on 07/09/2016.
 */
public class EmployeeTestFactory {

    public static List<Employee> createTestEmployees(int howMany) {
        ArrayList<Employee> employees = new ArrayList<>(howMany);
        for (int i = 0; i < howMany; i++) {
            employees.add(createEmployee(i));
        }
        return employees;
    }

    public static Employee createEmployee(int i) {
        return new Employee(createEmployeeProfile(i));
    }

    private static Profile createEmployeeProfile(int i) {
        return Profile.builder()
                      .avatar(String.format("http://test.com/avatar%d.jpg", i))
                      .bio("Bio " + i)
                      .dateOfBirth(new Date())
                      .email(String.format("user%d@test.com", i))
                      .hexColor("#FFFFFF")
                      .id(UUID.randomUUID().toString())
                      .isActive(true)
                      .name(createEmployeeName(i))
                      .build();

    }

    @NonNull
    private static Name createEmployeeName(int i) {
        return new Name("first" + i, "last" + i);
    }
}
