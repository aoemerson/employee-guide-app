package com.examplecompany.employees.presenter.main;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.examplecompany.employees.R;
import com.examplecompany.employees.api.EmployeesClient;
import com.examplecompany.employees.model.Employee;
import com.examplecompany.employees.util.EmployeeTestFactory;
import com.examplecompany.employees.view.main.OpenEmployeeDetailCommand;
import com.examplecompany.employees.view.main.EmployeesView;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class MainEmployeesPresenterTests {

    @Mock
    EmployeesClient employeesClient;

    @Mock
    EmployeesView employeesView;

    @Before
    public void setup() {

    }

    @Test
    public void loadAndDisplayEmployees() {
        MainEmployeesPresenter presenter = new MainEmployeesPresenter(employeesView, employeesClient);
        presenter.requestEmployees();
        verify(employeesView, times(1)).showLoading(eq(true));
        verify(employeesClient, times(1)).getEmployees();
    }

    @Test
    public void shouldUpdateViewOnEmployeesLoaded() {
        MainEmployeesPresenter presenter = new MainEmployeesPresenter(employeesView, employeesClient);

        List<Employee> testEmployees = EmployeeTestFactory.createTestEmployees(10);
        presenter.onEmployeesLoaded(new EmployeesClient.EmployeesLoadedEvent(testEmployees));
        verify(employeesView, times(1)).showLoading(eq(false));
        verify(employeesView, times(1)).showEmployees(eq(testEmployees));
    }

    @Test
    public void shouldShowErrorOnEmployeesLoadingException() {
        MainEmployeesPresenter presenter = new MainEmployeesPresenter(employeesView, employeesClient);
        Exception testException = new Exception();
        presenter.onEmployeesLoadError(new EmployeesClient.EmployeeLoadingErrorEvent(testException));
        verify(employeesView, times(1)).showLoading(eq(false));
        verify(employeesView, times(1)).showError(eq(R.string.error_msg_generic));
    }

    @Test
    public void shouldShowErrorOnEmployeesLoadingIOException() {
        MainEmployeesPresenter presenter = new MainEmployeesPresenter(employeesView, employeesClient);
        Exception testException = new IOException("Failed to connect to Ribot API");
        presenter.onEmployeesLoadError(new EmployeesClient.EmployeeLoadingErrorEvent(testException));
        verify(employeesView, times(1)).showLoading(eq(false));
        verify(employeesView, times(1)).showError(eq(R.string.error_msg_employee_connection_failed));
    }

    @Test
    public void shouldUpdateViewOnEmployeeClicked() {
        MainEmployeesPresenter presenter = new MainEmployeesPresenter(employeesView, employeesClient);
        presenter.employees = EmployeeTestFactory.createTestEmployees(5); // Inject for test purposes
        presenter.employeeClicked(1);
        verify(employeesView, times(1)).showEmployeeDetail(any(OpenEmployeeDetailCommand.class));
    }

    @Test
    public void shouldAttachAndDetachView() {
        MainEmployeesPresenter presenter = new MainEmployeesPresenter();
        assertThat(presenter.employeesView, is(nullValue()));
        presenter.attachView(employeesView);
        assertThat(presenter.employeesView, is(not(nullValue())));
        assertThat(employeesView, is(equalTo(presenter.employeesView)));

        presenter.detachView();
        assertThat(employeesView, is(not(equalTo(presenter.employeesView))));
        assertThat(presenter.employeesView, is(nullValue()));
    }

    @Test
    public void shouldShowErrorOnNoEmployees() {
        MainEmployeesPresenter presenter = new MainEmployeesPresenter(employeesView, employeesClient);
        presenter.onEmployeesLoaded(new EmployeesClient.EmployeesLoadedEvent(new ArrayList<Employee>()));
        int expectedErrMsgResId = R.string.error_msg_no_employees_found;
        verify(employeesView, times(1)).showError(eq(expectedErrMsgResId));
    }

    @Test
    public void shouldShowErrorOnNullEmployees() {
        MainEmployeesPresenter presenter = new MainEmployeesPresenter(employeesView, employeesClient);
        presenter.onEmployeesLoaded(null);
        int expectedErrMsgResId = R.string.error_msg_no_employees_found;
        verify(employeesView, times(1)).showError(eq(expectedErrMsgResId));
    }
}
