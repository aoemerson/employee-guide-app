package com.examplecompany.employees.api;

import java.util.List;

import com.examplecompany.employees.application.EventBus;
import com.examplecompany.employees.model.Employee;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EmployeesClientImpl implements EmployeesClient {


    private static EmployeesClientImpl instance;
    EmployeesService employeesService;

    private EmployeesClientImpl() {
        employeesService = EmployeesService.Creator.build();
    }

    public static EmployeesClientImpl getInstance() {
        if (instance == null) {
            instance = new EmployeesClientImpl();
        }
        return instance;
    }

    @Override
    public void getEmployees() {
        employeesService.getEmployees().enqueue(new Callback<List<Employee>>() {
            @Override
            public void onResponse(Call<List<Employee>> call, Response<List<Employee>> response) {
                if (response.isSuccessful()) {
                    EventBus.getInstance().post(new EmployeesLoadedEvent(response.body()));
                }
                else {
                    EventBus.getInstance().post(new EmployeeLoadingErrorEvent(null));
                }
            }

            @Override
            public void onFailure(Call<List<Employee>> call, Throwable t) {
                EventBus.getInstance().post(new EmployeeLoadingErrorEvent(t));
            }
        });
    }


}
